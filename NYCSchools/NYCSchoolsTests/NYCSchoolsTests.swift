//
//  NYCSchoolsTests.swift
//  NYCSchoolsTests
//
//  Created by Snehal Firodia on 01/04/19.
//  Copyright © 2019 Snehal Firodia. All rights reserved.
//

import XCTest
@testable import NYCSchools

class NYCSchoolsTests: XCTestCase {

     func testMockDataForSchoolInfo() {
        let responseData = ["dbn": "01M292",
                            "num_of_sat_test_takers": "29",
                            "sat_critical_reading_avg_score": "355",
                            "sat_math_avg_score": "404",
                            "sat_writing_avg_score": "363",
                            "school_name": "HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES"]
        let model = NYCSchoolInfo().parseSchoolInfo(responseData as NSDictionary)
        
        XCTAssertEqual(model.num_of_sat_test_takers, responseData["num_of_sat_test_takers"])
        XCTAssertEqual(model.sat_critical_reading_avg_score, responseData["sat_critical_reading_avg_score"])
        XCTAssertEqual(model.sat_math_avg_score, responseData["sat_math_avg_score"])
        XCTAssertEqual(model.sat_writing_avg_score, responseData["sat_writing_avg_score"])
        XCTAssertEqual(model.school_name, responseData["school_name"])
    }
    
    func testCreateSchoolInfoWhenGivenEmptyData(){
        //Given: Empty data
        
        //When
        let model = NYCSchoolInfo().parseSchoolInfo([:])
        
        //Then
        XCTAssertNotNil(model)
    }

}
