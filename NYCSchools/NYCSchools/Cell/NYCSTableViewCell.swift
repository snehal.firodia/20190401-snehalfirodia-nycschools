//
//  NYCSTableViewCell.swift
//  NYCSchools
//
//  Created by Snehal Firodia on 29/03/19.
//

import UIKit

//NYCSTableViewCell is the custom UITableViewCell class.
class NYCSTableViewCell: UITableViewCell {
    
    static let cellIdentifier = "NYCSTableViewCell"
    
    @IBOutlet fileprivate weak var school_Name: UILabel!
    
    override func awakeFromNib() {
        layer.cornerRadius = 12
        layer.masksToBounds = true
    }
    
    func fill(_ schoolName: String){
        school_Name.text = schoolName
    }
}
