//
//  NYCSchoolNetworkManager.swift
//  NYCSchools
//
//  Created by Snehal Firodia on 29/03/19.
//

import Foundation

//The NYCSchoolNetworkManager class is designed to perform network calls and fetch the data.
//This is a singleton class.

public class NYCSchoolNetworkManager: NSObject {
    
    @objc public static let sharedManager = NYCSchoolNetworkManager()
    
    // Then callGetService() methods performs the network operations.
    // The parameters to the function are:
    // param url: The request url
    // param completion handler: The completion handler returns the jsonserialised data array and the success parameter to the calling class.
    // @objc attribute is added to make the function available for objective-C classes.
    
    @objc public func callGetService(url:String, completion: @escaping (_ data: NSArray,_ success: Bool) -> ()) {
        let request = NSMutableURLRequest(url: URL(string:url)!)
        let session = URLSession.shared
        request.httpMethod = "GET"
        request.addValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        
        let task = session.dataTask(with: request as URLRequest) { data, response, error in
            guard data != nil else {
                print("No data found: \(String(describing: error))")
                completion(NSArray(), false)
                return
            }
            do {
                if let json = try JSONSerialization.jsonObject(with: data!, options:[]) as? NSArray {
                    completion(json, true)
                } else {
                    let jsonStr = NSString(data: data!, encoding: String.Encoding.utf8.rawValue) //No error thrown, but not NSArray
                    print("Error: Could not parse JSON: \(String(describing: jsonStr))")
                    completion(NSArray(), false)
                }
            } catch let parseError {
                print(parseError)
                let jsonStr = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                print("Error: Could not parse JSON: '\(String(describing: jsonStr))'")
                completion(NSArray(), true)
            }
        }
        
        task.resume()
    }
}
