//
//  NYCSchools.m
//  NYCSchools
//
//  Created by Snehal Firodia on 29/03/19.
//

#import <Foundation/Foundation.h>
#import "NYCSchools.h"
#import <objc/runtime.h>
#import "NYCSchools-Swift.h"

@implementation NYCSchools

- (void)deserializeArray:(NSString *)name dict:(NSDictionary *)dict {
    NSMutableArray* arrayOfObjects = [[NSMutableArray alloc] init];
    for (NSObject *object in [dict objectForKey:name]) {
        if([object isKindOfClass:[NSString class]] || [object isKindOfClass:[NSNumber class]])
        {
            [arrayOfObjects addObject:object];
        }
        else if([object isKindOfClass:[NSArray class]])
        {
            [self deserializeArray:name dict:dict];
        }
        else if([object isKindOfClass:[NSDictionary class]])
        {
            id arrayOfObject = [[NSClassFromString(name) alloc] initWithDictionary:(NSDictionary*)object];
            [arrayOfObjects addObject:arrayOfObject];
        }
    }
    [self setValue:arrayOfObjects forKey:name];
}

- (void)deserializeDictionary:(NSString *)name dict:(NSDictionary *)dict
{
    for (NSObject *object in [dict objectForKey:name]) {
        if([object isKindOfClass:[NSString class]] || [object isKindOfClass:[NSNumber class]])
        {
            [self setValue:[dict objectForKey:name] forKey:name];
        }
        else if([object isKindOfClass:[NSArray class]])
        {
            [self deserializeArray:name dict:dict];
        }
        else if([object isKindOfClass:[NSDictionary class]])
        {
            id arrayOfObject = [[NSClassFromString(name) alloc] initWithDictionary:(NSDictionary*)object];
            [self setValue:arrayOfObject forKey:name];
        }
    }
}


-(id)initWithDictionary:(NSDictionary*)dict {
    self = [self init];
    if(self) {
        unsigned count;
        objc_property_t *properties = class_copyPropertyList([self class], &count);
        unsigned i;
        for (i = 0; i < count; i++)
        {
            objc_property_t property = properties[i];
            NSString *name = [NSString stringWithUTF8String:property_getName(property)];
            
            const char * type = property_getAttributes(property);
            
            NSString * typeString = [NSString stringWithUTF8String:type];
            NSArray * attributes = [typeString componentsSeparatedByString:@","];
            NSString * typeAttribute = [attributes objectAtIndex:0];
            
            Class typeClass = nil;
            if ([typeAttribute hasPrefix:@"T@"] && [typeAttribute length] > 1) {
                NSString * typeClassName = [typeAttribute substringWithRange:NSMakeRange(3, [typeAttribute length]-4)];  //turns @"NSDate" into NSDate
                typeClass = NSClassFromString(typeClassName);
                if (typeClass == nil) {
                    
                    // Here is the corresponding class even for nil values
                }
            }
            if([NSStringFromClass(typeClass) isEqualToString:@"NSString"]|| [NSStringFromClass(typeClass) isEqualToString:@"NSNumber"]) {
                [self setValue:[dict objectForKey:name] forKey:name];
            }
            else if([NSStringFromClass(typeClass) isEqualToString:@"NSArray"])
            {
                [self deserializeArray:name dict:dict];
            }
            else if([NSStringFromClass(typeClass) isEqualToString:@"NSDictionary"])
            {
                [self deserializeDictionary:name dict:dict];
                
            }
        }
    }
    return self;
}

-(void)fetchData:(void (^)(NSArray* schoolsData, BOOL success))finishBlock {
    NSMutableArray* schoolsData = [[NSMutableArray alloc] init];
    NYCSchoolNetworkManager *nyNM = [NYCSchoolNetworkManager sharedManager];
    NSString* urlString = [Constant schoolUrl];
    [nyNM callGetServiceWithUrl:urlString completion:^(NSArray *response,BOOL success) {
        if (success) {
            for (NSDictionary* school in response)  {
                NYCSchools* schoolData = [[NYCSchools alloc] initWithDictionary:school];
                [schoolsData addObject:schoolData];
            }
            finishBlock(schoolsData,true);
        }
        else {
            finishBlock(nil, false);
        }
    }];
}

@end
