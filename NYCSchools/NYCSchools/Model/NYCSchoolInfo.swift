//
//  NYCSchoolInfo.swift
//  NYCSchools
//
//  Created by Snehal Firodia on 30/03/19.
//

import Foundation

//NYCSchoolInfo is model class for maintaining the sat score data for particular school.
class NYCSchoolInfo {
    var dbn:NSNumber?
    var num_of_sat_test_takers: String?
    var sat_critical_reading_avg_score: String?
    var sat_math_avg_score: String?
    var sat_writing_avg_score: String?
    var school_name: String?
    
    //This function fetches data from the network manager class and passes the model to the viewcontroller.
    func fetchSchoolInfo(_ dbn: NSNumber, completion: @escaping (_ schoolInfo: NYCSchoolInfo,_ success: Bool) -> ()) {
        let urlString = String(format: schoolDataUrl, dbn)
        
        NYCSchoolNetworkManager.sharedManager.callGetService(url: urlString) { (responseData, success) in
            if success {
                if responseData.count > 0 {
                    if let schoolInfo = responseData[0] as? NSDictionary {
                        let object = self.parseSchoolInfo(schoolInfo)
                        completion(object, true)
                    }
                }
                else {
                    completion(NYCSchoolInfo(), false)
                }
            }
            else {
                completion(NYCSchoolInfo(), false)
            }
        }
    }
    
    //This function parses the dictionary and stores into the object model.
    func parseSchoolInfo(_ dictionary: NSDictionary) -> NYCSchoolInfo {
        let schoolInfo = NYCSchoolInfo()
        if let criticalReadingScore = dictionary["sat_critical_reading_avg_score"] as? String {
            schoolInfo.sat_critical_reading_avg_score = criticalReadingScore
        } else {
           schoolInfo.sat_critical_reading_avg_score = "NA"
        }
        
        if let testTakers = dictionary["num_of_sat_test_takers"] as? String {
            schoolInfo.num_of_sat_test_takers = testTakers
        } else {
            schoolInfo.num_of_sat_test_takers = "NA"
        }
        
        if let mathScore = dictionary["sat_math_avg_score"] as? String {
            schoolInfo.sat_math_avg_score = mathScore
        } else {
            schoolInfo.sat_math_avg_score = "NA"
        }
        
        if let writingScore = dictionary["sat_writing_avg_score"] as? String {
            schoolInfo.sat_writing_avg_score = writingScore
        } else {
            schoolInfo.sat_writing_avg_score = "NA"
        }
        
        if let schoolName = dictionary["school_name"] as? String {
            schoolInfo.school_name = schoolName
        } else {
            schoolInfo.school_name = "NA"
        }

        schoolInfo.num_of_sat_test_takers = dictionary["num_of_sat_test_takers"] as? String
        return schoolInfo
    }
}
