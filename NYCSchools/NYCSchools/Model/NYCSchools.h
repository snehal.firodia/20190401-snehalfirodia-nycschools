//
//  NYCSchools.h
//  NYCSchools
//
//  Created by Snehal Firodia on 29/03/19.
//

#import <Foundation/Foundation.h>
//The below line is added to access properties and functions of NYCSchoolNetworkManager.swift file in this objective-C file.
@class NYCSchoolNetworkManager;


//NYCSchools is model class for maintaining the data of high schools.
@interface NYCSchools: NSObject

//Below is basic init function for model
-(id)initWithDictionary:(NSDictionary*)dict;

//This function fetches data from the network manager class and passes to the viewcontroller.
-(void)fetchData:(void (^)(NSArray* schoolsData, BOOL success))finishBlock;

//This property stores school name.
@property (nonatomic, strong) NSString *school_name;

//This property stores school email.
@property (nonatomic, strong) NSString *school_email;

//This property stores school website.
@property (nonatomic, strong) NSString *website;

//This property stores school dbn.
@property (nonatomic, strong) NSNumber *dbn;

//This property stores school address.
@property (nonatomic, strong) NSString *primary_address_line_1;

//This property stores school phone number.
@property (nonatomic, strong) NSString *phone_number;
@end
