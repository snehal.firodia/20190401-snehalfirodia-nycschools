//
//  Constants.swift
//  NYCSchools
//
//  Created by Snehal Firodia on 29/03/19.
//

import Foundation

//The below constants are used in Swift files.
let schoolsUrl = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
let schoolDataUrl = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn=%@"

//For using global constants in Objective-C files, the following class is been used.
@objc class Constant: NSObject {
    private override init() {}
    
    @objc class func schoolUrl() -> String { return schoolsUrl }
    @objc class func schoolsDataUrl() -> String { return schoolDataUrl }
}
