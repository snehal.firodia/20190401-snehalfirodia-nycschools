//
//  ViewController.swift
//  NYCSchools
//
//  Created by Snehal Firodia on 29/03/19.
//

import UIKit

//NYCSViewController is the base controller for the application.
//It shows the list of NYC High Schools.
class NYCSViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet fileprivate weak var tableView: UITableView!
    var schoolsData = [NYCSchools]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        registerCellForTable()
        fetchData()
    }
    //func registerCellForTable registers the custom UITableViewCell.
    func registerCellForTable() {
        self.tableView.register(UINib(nibName: String(describing: NYCSTableViewCell.self), bundle: Bundle(for: type(of: self))), forCellReuseIdentifier: String(describing: NYCSTableViewCell.self))
    }
    
    //func fetchData(), fetches the data for the model object NYCSchools and shows the alert controller if any error thrown from the server.
    func fetchData() {
        showActivityIndicator()
        let nyschools = NYCSchools()
        nyschools.fetchData { (_ schoolData: [Any]?, success: Bool) -> Void in
            if success {
                self.schoolsData = schoolData as! [NYCSchools]
                DispatchQueue.main.async {
                    self.hideActivityIndicator()
                }
            }
            else {
                DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
                let alert = UIAlertController.init(title: "Error", message: "The data of NYC High Schools cannot be loaded.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                }
            }
        }
        NYCSchoolNetworkManager.sharedManager.callGetService(url: schoolsUrl) { (response, success) in
            if success {
                for school in response {
                    let schools = NYCSchools.init(dictionary: school as? NSDictionary as? [AnyHashable : Any])
                    guard let schoolData = schools else {
                        return
                    }
                    self.schoolsData.append(schoolData)
                }
                DispatchQueue.main.async {
                    self.hideActivityIndicator()
                }
            }
            else {
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                let alert = UIAlertController.init(title: "Error", message: "The data of NYC High Schools cannot be loaded.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
}

//extension NYCSViewController: This extension contains the functions for UITableViewDelegate and UITableViewDataSource
extension NYCSViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schoolsData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: NYCSTableViewCell.self), for: indexPath)
        guard let modalsCell = cell as? NYCSTableViewCell else { return UITableViewCell(style: .default, reuseIdentifier: "cell")}
        modalsCell.selectionStyle = .none
        modalsCell.fill(schoolsData[indexPath.row].school_name)
        modalsCell.separatorInset = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        modalsCell.layer.borderWidth = 5
        modalsCell.layer.borderColor = UIColor.white.cgColor

        return modalsCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let detailController = storyboard.instantiateViewController(withIdentifier: "NYCSDetailInfoViewController") as? NYCSDetailInfoViewController else {
            return
        }
        detailController.nycSchool = schoolsData[indexPath.row]
        
        navigationController?.pushViewController(detailController, animated: true)
    }
}

//extension NYCSViewController: This extension contains the custom methods to show and hide activity indicator.
extension NYCSViewController {
    func showActivityIndicator() {
        activityIndicator.startAnimating()
        tableView.alpha = 0
        titleLabel.alpha = 0
    }
    
    func hideActivityIndicator() {
        activityIndicator.stopAnimating()
        tableView.alpha = 1.0
        titleLabel.alpha = 1.0
        tableView.reloadData()
    }
}
