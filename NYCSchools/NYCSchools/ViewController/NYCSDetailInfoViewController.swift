//
//  NYCSDetailInfoViewController.swift
//  NYCSchools
//
//  Created by Snehal Firodia on 30/03/19.
//

import Foundation
import UIKit

class NYCSDetailInfoViewController: UIViewController {
    
    var nycSchool: NYCSchools?
    var nycSchoolInfo: NYCSchoolInfo?
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var critical_writing_score: UILabel!
    @IBOutlet weak var writing_score: UILabel!
    @IBOutlet weak var maths_score: UILabel!
    @IBOutlet weak var school_website: UIButton!
    @IBOutlet weak var school_email: UIButton!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var contact: UIButton!
    @IBOutlet weak var school_name: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    //The function to call the contact clicked.
    @IBAction func contactClicked(_ sender: Any) {
        let button = sender as? UIButton
        let contact = String(describing: button?.titleLabel?.text)
        if let url = URL(string: "tel://\(contact)") {
            if UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
            else {
                let alert = UIAlertController.init(title: "Error", message: "The call feature is not supported.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    //The function to open the website clicked.
    @IBAction func websiteClicked(_ sender: Any) {
        let button = sender as? UIButton
        guard let url = URL(string: (button?.titleLabel?.text)!) else {
            return //be safe
        }
        if UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
            else {
                UIApplication.shared.openURL(url)
            }
        }else {
            let alert = UIAlertController.init(title: "Error", message: "The application could not load url.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //The function to mail the email id clicked.
    @IBAction func emailClicked(_ sender: Any) {
        let button = sender as? UIButton
        let email = String(describing: button?.titleLabel?.text)
        
        if let url = URL(string: "mailto:\(email)") {
            if UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url)
                }
                else {
                    UIApplication.shared.openURL(url)
                }
            }else {
                let alert = UIAlertController.init(title: "Error", message: "The application could not open mail.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        title = "School Details"
        fetchSchoolData()
        scrollView.delegate = self
    }
    
    func fetchSchoolData() {
        showActivityIndicator()
        NYCSchoolInfo().fetchSchoolInfo((nycSchool?.dbn!)!) { (schoolInfo, success) in
            if success {
                self.nycSchoolInfo = schoolInfo
                DispatchQueue.main.async {
                    self.hideActivityIndicator()
                }
            } else {
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                    let alert = UIAlertController.init(title: "Error", message: "The data of NYC High Schools cannot be loaded.", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    func showInfo() {
        critical_writing_score.text = nycSchoolInfo?.sat_critical_reading_avg_score
        writing_score.text = nycSchoolInfo?.sat_writing_avg_score
        maths_score.text = nycSchoolInfo?.sat_math_avg_score
        school_website.setTitle(nycSchool?.website, for: .normal)
        school_email.setTitle(nycSchool?.school_email, for: .normal)
        school_name.text = nycSchool?.school_name
        address.text = nycSchool?.primary_address_line_1
        contact.setTitle(nycSchool?.phone_number, for: .normal)
    }
}

extension NYCSDetailInfoViewController {
    func showActivityIndicator() {
        activityIndicator.startAnimating()
        stackView.alpha = 0.0
    }
    
    func hideActivityIndicator() {
        activityIndicator.stopAnimating()
        showInfo()
        stackView.alpha = 1.0
    }
}

extension NYCSDetailInfoViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let scrollSize = CGSize(width: UIScreen.main.bounds.width, height: scrollView.contentSize.height)
        scrollView.contentSize = scrollSize
    }
}
